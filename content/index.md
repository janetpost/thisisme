---
title: "This Is Janet Post"
---

## Janet Post's ReadMe

I am the Team Lead for the Linux Team at JDPA.  This page is inspired by the Team Readme pages at Gitlab.  This is much harder than it looks!


### Goals
---


#### My Personal Goal:
To create a safe environment for you to:

* Succeed
* Fail
* Disagree
* Offer suggestions
* Provide feedback -- negative or postive
* Be yourself
* Grow and learn

#### To accomplish these goals:

**Routine**

* **Daily Standups**
  * Due to the staggered start of our team, we have a digital TEAMS channel for our daily standup, rather than physically standing somewhere.
  * Short and Sweet listing of what you expect to be your highest priority items for the day.
  * List any blockers and solicit assistance from other team members.
  * Do not use this for any extended conversations.  Please use the Linux TEAMS channel for longer conversations.
  * Duration:  30 secs.   1 minute if you type slowly
* **Weekly team meetings**
  * Weekly meeting for us to get together and hash out things as a group
  * This meeting will be digital moving forward so our remote workers are not forgotten on the conference room speaker.  (Also, we lost our meeting room)
  * Duration: 1 hour.
* **One on Ones**
  * We can walk, sit, eat, whatever is most comfortable for you.
  * We can talk about goals, projects, road blocks, your cat.  Whatever is important and pressing for you.
  * Confidential, of course.  Everything that happens in the one on one stays in the one on one.
  * Duration: 1/2 hour or however long we need.
  * _DO NOT WAIT FOR THE ONE ON ONE_
* **Quarterly Reviews**
	* I dislike doing quarterly reviews.  They are a corporate evil.
	* Nothing will ever come as a surprise to you because I do not beleive in waiting for the quarterly review to address things.  Everything we talk about here will have already been addressed in one on one meetings.

**Philosophy**

* **Open Door Policy:** Do not wait for the one on one.  If you have something you need to talk about, we'll talk.  You are always my number one priority.  We can find a private room, we can go for a walk and talk, whatever you are comfortable with.
* **Mistakes happen:**  I care more that we learn from them and find ways to prevent them in the future than assigning blame.
* **Distaste for Bandaids:** I would rather do things the 'right way' the first time, rather than apply bandaid solutions that will require even more effort to remove later.
* **I Trust You:**  
  * To make informed and intelligent decisions  
  * To be great at what you do  
  * To push back  
  * To escalate if appropriate  

### Cross Training
---
I am a big believer in cross-training and sharing knowledge, especially for large projects.  I will routinely ask for two volunteers for projects:

  * **Subject Matter Learner:**  I will ask the person with the _least_ knowledge to be on point for projects.  They will write the automation, and perform the deployments, effectively testing the documentation for missing or assumed knowledge.
  * **Subject Matter Expert:** I will ask the person with the most expertise to be on hand to advise, to write the documentation, and to be back up for the point person.
  * Knowledge share should also happen at the weekly meeting.
  * Once a product is close to production ready, I would ask someone else on the team perform a deployment to test the documentation.

### Communication
---
I am usually always available, even outside of normal office hours.  I like to spend my mornings with my husband, walking, cooking, or just watching the sunrise.  You can call me on my cellphone anytime between 8am and 6pm (EST).  I do not mind talking when I am in the car.   The hour after I get home is dedicated to my family.  Outside of this, if I am on my computer (which I am a lot) I am able to talk via TEAMS.

**Non-emergencies:**  
  * I prefer TEAMS messages.  I have TEAMS on my phone and my home computer.  
  * I may not answer immediately.  
  * I do not expect you to answer immediately or outside of office hours, either.  

**Emergencies:**  
  * I still prefer TEAMS.  Ping me multiple times.   
  * Barring TEAMS, call me.  My ringer is always off, but my watch vibrates on my wrist. If I do not answer, call back again right away.  I am likely looking for my phone.  
  * _Do not leave a voice mail_.  In fact, if you listen to my voice mail message, it will tell you that I never check it.   
  * Email might work. I do not check it after hours and I get no notifications on my phone/watch.  

### Weaknesses
---
  * I like to talk and chit chat.  This is how I connect with my co-workers.  I sometimes lose track of the time and can get lost in the conversation, so please feel free let me know.  I will not be offended.
  * I tend to jump to solutioning before a problem is clearly defined.  I am working on this.  If I belabour the task of defining a problem and it's scope, this is me over-compensating.

### Fun Facts
---
  * I have been married for 21 years, I have five children, and 3 cats.
  * I live in the country, 45 minutes away from everything.
  * I have told my children, since they were old enough to ask, that I am 25.  This is becoming awkward as my eldest son turned 20.  I honestly do not remember how old I really am.
  * I cannot math.  I will use a calculator.  Every time.
  * I enjoy public speaking and am an active member of the Autodata Toastmasters Club.  I would love to give a Ted Talk one day.
  * My favourite video games are Elder Scrolls Online, 7 Days to Die, and Farming Simulator
  * My favourite board game is Talisman.  I own all the expansions for the latest edition.  At my house, a single game can last more a week because we play to experience the game, not necessarily to race to the finish.  One time I was a 'Toad' for an entire day.  I still won.

### Techno Fun Facts
---
  * I have worked in Information Technologies since 1995. My first job was selling and building computers to spec from a store front after school.
  * I programmed a Star Trek game in BASIC on the Vic20 when I was in grade 4.  My dad had to teach me multiplication and division.
  * I can program operational PERL and BASH.
  * My second day working for an ISP,  I knocked all of our clients off the internet when I accidently tripped a cisco bug that seg-faulted both the primary and secondary core network routers.

